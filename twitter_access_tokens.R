## Creating a Twitter access token from an API key and saving it as an environment variable for use with rtweet


library(rtweet)

## Enter whatever name you assigned to your created app
appname <- "my_app"

## Your consumer key/API key (example below is not a real key)
key <- "XYznzPFOFZR2a39FwWKN1Jp41"

## Your consumer secret/API secret (example below is not a real key)
secret <- "CtkGEWmSevZqJuKl6HHrBxbCybxI1xGLqrD5ynPd9jG0SoHZbD"

## create token named "twitter_token"
twitter_token <- create_token(app = appname,
                              consumer_key = key,
                              consumer_secret = secret)

## Saving tokens

## The token created above can be entered as an argument into various rtweet functions. However, this requires you to create a token in every new R session and specify the token in each function. A simpler approach is to save the token to your home directory and set it as an environment variable.

## First, find the path of your home directory
home_directory <- path.expand("~/")

## Combine your home directory with the name for your token
file_name <- paste0(home_directory, "twitter_token.rds")

## Save the token to your home directory
saveRDS(twitter_token, file = file_name)


## Now create a text file containing the path to the token and save it as an .Renviron variable.
cat(paste0("TWITTER_PAT=", file_name), 
    file = file.path(home_directory, ".Renviron"),
    append = TRUE)


## Be sure to reload the .Renviron to initialize everything.
readRenviron("~/.Renviron")
