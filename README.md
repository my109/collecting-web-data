# Collecting Web-Based Data using R

These materials accompany the DuPRI "Collecting Web-Based Data" training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* collecting_web_data.Rmd - Rmarkdown HTML version of training code
* collecting_web_data.html - HTML report of training for reference
* Collecting Web Data.Rproj - RStudio project file
* twitter_access_tokens.R - R script file to create and save Twitter access tokens for use with `rtweet` package